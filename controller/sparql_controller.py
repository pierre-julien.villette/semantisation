"""
Sparql controller

This module manages the sparql queries

Functions:
     - execute_query(sparql_query): execute a sparql query
     - save_query(query, query_name, query_desc=None): save a sparql query
     - remove_query(query_name): remove a sparql query
     - get_query(data): return sparql query from its name
     - list_queries(query, description): list of saved sparql queries
"""

from rdflib.plugins.sparql import prepareQuery
from rdflib import Graph
from utils import configuration
from model import (
    owl,
    sparql_query,
)

config = configuration.getconfigs()

sio = owl.Owl()

graph = Graph()
graph.parse(sio.get_path())

session = sparql_query.Session()
sparql_query.Base.metadata.create_all(sparql_query.engine)


def execute_query(sparql_query):
    """
    execute a sparql query
    :param sparql_query: sparql query
    :return: number of rows, rows
    """
    query = prepareQuery(sparql_query, initNs={})
    results = graph.query(query)
    return len(results), results


def save_query(query, query_name, query_desc=None):
    """
    save a sparql query
    :param query: sparql query
    :param query_name: sparql query name
    :param query_desc: sparql query description
    :return: True
    """
    s_query = sparql_query.SparqlQuery(
        query=query,
        name=query_name,
        description=query_desc
    )

    session.merge(s_query)
    session.commit()

    return True


def remove_query(query_name):
    """
    remove a sparql query
    :param query_name: sparql query name
    :return: True
    """

    #Todo: case of unknown query_name to deal with
    query = session.query(sparql_query.SparqlQuery) \
        .filter(sparql_query.SparqlQuery.name == query_name)
    if session.query(query.exists()):
        session.delete(query.first())
        session.commit()

    return True


def get_query(query_name):
    """
    return sparql query from its name
    :param query_name: sparql query name
    :return: sparql query
    """
    query = session.query(sparql_query.SparqlQuery) \
        .filter(sparql_query.SparqlQuery.name == query_name)

    if session.query(query.exists()):
        result = query.first().query
    else:
        result = None

    return result


def list_queries(query=False, description=True):
    """
    list of saved sparql queries
    :param query: whether queries should be included
    :param description: whether descriptions should be included
    :return: list
    """
    result = {}

    if query and description:
        for instance in session.query(sparql_query.SparqlQuery):
            result[instance.name] = (instance.query, instance.description)
    elif query:
        for instance in session.query(sparql_query.SparqlQuery):
            result[instance.name] = instance.query
    elif description:
        for instance in session.query(sparql_query.SparqlQuery):
            result[instance.name] = instance.description
    else:
        for instance in session.query(sparql_query.SparqlQuery):
            result[instance.name] = None

    return result
