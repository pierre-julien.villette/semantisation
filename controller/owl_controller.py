"""
Owl controller

This module manages the ontology

Functions:
     - json2owl(json_file, owl_file, reasoner = True): complete the ontology
        from the json file
     - launch_reasoner(onto): launch reasoner
"""
import json
from datetime import datetime

from owlready2 import (
    get_ontology,
    sync_reasoner,
    OwlReadyInconsistentOntologyError)

from utils import configuration
from controller import sparql_controller as spa
from model import owl

config = configuration.getconfigs()

ontology = owl.Owl()
owl_path = ontology.get_path()
sio = get_ontology(owl_path).load()
graph = spa.graph


def json2owl(json_file, owl_file, reasoner=False):
    """
    complete the ontology from the json file
    :param json_file:
    :param owl_file:
    :param reasoner:
    :return: True if ok
    """

    with open(json_file, 'r') as file:
        docs = json.load(file)

    # dictionary to avoid duplicates
    dict_documents = {}

    i=0
    for data in docs:
        identifier = data['identifier']
        i+=1
        print(i)

        if identifier in dict_documents:
            document = dict_documents[identifier]
        else:
            document = sio.Document()
            document.identifier.append(identifier)
            dict_documents[identifier] = document

        document.title.append(data['title'])
        document.subject.append(data['subject'])

        document.created.append(
            datetime.fromisoformat(data['created'])
        )

        # adding authors
        for agent in data['maker']:
            name = agent['name'].replace(' ', '_')

            author = sio.Agent(
                name=name
            )

            document.maker.append(author)
            author.made.append(document)

        # adding citations
        citations = data['bibliographicCitation']
        if citations is not None:
            for citation in citations:
                citation_id = citation

                if citation_id in dict_documents:
                    citation_doc = dict_documents[citation_id]
                else:
                    citation_doc = sio.Document(
                        identifier=citation_id
                    )
                    dict_documents[citation_id] = citation_doc
                document.bibliographicCitation.append(citation_doc)

    result = True

    # consistency check if requested
    if reasoner:
        result = launch_reasoner(sio)

    # if no problem then save, refresh the graph and free the memory
    if result:
        sio.save(owl_file)
        graph.parse(owl_file)
        sio.destroy()

    return result


def launch_reasoner(onto):
    """
    launch reasoner
    :param onto: ontology
    :return: true if no inconsistencies
    """
    try:
        sync_reasoner(x=onto, infer_property_values=True, debug=True,
                      keep_tmp_file=True)
        result = True
    except OwlReadyInconsistentOntologyError():
        result = False
    finally:
        return result
