"""
configuration module

This module manages the configuration of this application

Functions:
     - getconfigs : Return properties

"""
import os
from jproperties import Properties


def getconfigs():
    """
    Return properties
    :return: properties
    :rtype: Properties
    """
    configuration = Properties()
    config_path = os.path.abspath(os.getcwd())

    try:
        with open(config_path + "/app-config.properties", "rb") as config_file:
            configuration.load(config_file)
    finally:
        config_file.close()
    return configuration


configs = getconfigs()
