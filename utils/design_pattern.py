"""
design pattern module

This module manages the design pattern

Class:
     - Singleton: design pattern guaranteeing a single instance
"""


class Singleton(type):
    """
    Metaclass
    Design pattern guaranteeing a single instance

    """

    _instances = {}

    def __call__(cls, *args, **kwargs):
        """
        call function reimplementation
        :param args:
        :param kwargs:
        :return:
        """

        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]
