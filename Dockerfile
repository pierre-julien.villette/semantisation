FROM python:3.11-alpine

WORKDIR /Ontology

COPY requirements.txt .
COPY api.py .
COPY app-config.properties .
COPY controller/ /Ontology/controller/
COPY model/ /Ontology/model/
COPY utils/ /Ontology/utils/
COPY _temp/ /Ontology/_temp/

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 5000

CMD ["python", "api.py"]
