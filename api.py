"""
api module

This module manages the API

Functions:
     - add_file(): API to add json file to ontology
     - sparql(): API to run a sparql query
     - list_queries(): API to list saved queries
     - view_query(): API to view a sparql query
"""

from flask import (
    Flask,
    request,
    abort,
    redirect,
    jsonify,
)

from utils import configuration
import controller.owl_controller as owl
import controller.sparql_controller as spa

app = Flask(__name__)
config = configuration.getconfigs()
deposit_in = config.get('DEPOSIT_IN').data
deposit_out = config.get('DEPOSIT_OUT').data
owl_file = config.get('OWL_FILE').data
server = config.get('SERVER').data


@app.route("/owl/add_file", methods=['GET'])
def add_file():
    """
    API to add json file to ontology

    API param file_name: file name in deposit

    :return: result in json format
    """
    file_in = deposit_in + request.args.get('file_name')
    file_out = deposit_out + owl_file

    reasoner = request.args.get('reasoner', default=True)
    if str(reasoner).lower() == 'false':
        reasoner = False
    else:
        reasoner = True

    print(file_in, ' - ', file_out, ' - ', reasoner)

    result = owl.json2owl(file_in, file_out, reasoner)

    data = {'result': result}
    return jsonify(data)


@app.route("/sparql/query", methods=['GET', 'POST'])
def sparql():
    """
    API to run a sparql query

    API param query: sparql query to run
    API param query_name: sparql query name in case of save or remove
    API param query_desc: sparql query description if save
    API param query_save: save sparql query
    API param query_remove: remove sparql query
    API param **kwargs: value of any sparql query parameters

    :return: result in json format
    """

    if request.method == 'POST':
        data = request.form
    else:
        data = request.args

    sparql_query = None
    if 'query' in data:
        sparql_query = data['query']

    response = None
    if 'query_name' in data:
        query_name = data['query_name']
        if 'query_save' in data:
            if str(data['query_save']).lower() == 'true':
                query_desc = data['query_desc']
                spa.save_query(sparql_query, query_name, query_desc)
                response = jsonify({query_name: 'saved'})
        elif 'query_remove' in data:
            if str(data['query_remove']).lower() == 'true':
                spa.remove_query(query_name)
                response = jsonify({query_name: 'removed'})
        else:
            sparql_query = spa.get_query(query_name)

    if response is None:
        if sparql_query is not None:
            try:
                sparql_query = sparql_query.format(**data)

                nb_results, results = spa.execute_query(sparql_query)

                response = {'nb': nb_results}
                for i, row in enumerate(results):
                    response[str(i)] = row
                response = jsonify(response)
            except KeyError as err:
                abort(400, description=err)
                response = redirect(request.url)
        else:
            abort(400, description='Request Error')
            response = redirect(request.url)

    return response


@app.route("/sparql/list_queries", methods=['GET'])
def list_queries():
    """
    API to list saved queries

    :return: list of names and descriptions in json format
    """
    response = spa.list_queries()

    return jsonify(response)


@app.route("/sparql/view_query", methods=['GET', 'POST'])
def view_query():
    """
    API to view a sparql query

    API param query_name: sparql query name

    :return: sparql query in json format
    """
    if request.method == 'POST':
        data = request.form
    else:
        data = request.args

    if 'query_name' in data:
        response = spa.get_query(data['query_name'])
    else:
        response = {}

    return jsonify(response)


if __name__ == "__main__":
    host = server.split(':')[0]
    port = server.split(':')[1]
    app.run(host=host, port=port, debug=True)
