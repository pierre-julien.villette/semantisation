"""
sparkl query module

This module manages the spark query

Class:
     - SparqlQuery: query to save
"""
from sqlalchemy import Column, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from utils import configuration

configs = configuration.getconfigs()

engine = create_engine("sqlite:///" + configs.get("DB_PATH").data)
Session = sessionmaker(bind=engine)
session = Session()

Base = declarative_base()


class SparqlQuery(Base):
    """
    Sparql query class

    :param name : query name
    :param query : query detail
    :param description : query description
    """
    __tablename__ = "T_sparql_query"
    name = Column(String, primary_key=True)
    query = Column(String)
    description = Column(String)
