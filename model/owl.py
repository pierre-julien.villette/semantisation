"""
owl module

This module manages the ontology

Class:
     - Owl: ontology
"""
import os

from utils import (
    design_pattern as dp,
    configuration,
    )
from owlready2 import get_ontology, OwlReadyOntologyParsingError


config = configuration.getconfigs()

deposit_out = config.get('DEPOSIT_OUT').data
owl_file = config.get('OWL_FILE').data
owl_path = config.get('ONTOLOGY_BASE').data


class Owl(metaclass=dp.Singleton):
    """
    Owl Class with unique instance

    Functions:
     - get_instance(self, init=False): returns the unique instance of the
     ontology
     - get_path(self, init=False): returns the path of owl file
    """

    def get_instance(self, init=False):
        """
        returns the unique instance of the ontology
        :param init: structure without data
        :return:
        """

        path = self.get_path(init)

        return get_ontology(path).load()

    def get_path(self, init=False):
        """
        returns the path of owl file
        :param init: structure without data
        :return:
        """

        result = owl_path

        if not init:
            path = deposit_out + owl_file
            if os.path.isfile(path):
                if os.path.getsize(path) > 0:
                    try:
                        get_ontology(path).load()
                        result = path
                    except OwlReadyOntologyParsingError:
                        print(path + ' is not a owl file')

        return result
