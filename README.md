# Ontology
_version 1.0 du 21/03/2023_

_par SIO_

## Présentation générale

Cette application propose une API permettant de sémantiser les données issues du TLA

## Services proposés

L'API propose 4 actions métier :

- [ ] __add file__: enrichissement de l'ontologie
- [ ] __sparql__: exécution d'une requête sparql
- [ ] __list queries__: liste des requêtes enregistrées
- [ ] __view query__: visualisation d'une requête enregistrée

L'action sparql permet d'exécuter une requête mais aussi de l'enregistrer et de supprimer son enregistrement.
Les requêtes peuvent être paramétrées. Les paramètres doivent être encadrés par des accolades et définient dans les variables de la requêtes.

## Contrat API

voir _API\_Reference.md_

### Configuration

Configurer l'application en ajustant les paramètres du fichier Ontology/app-config.properties:

- [ ] __ONTOLOGY_BASE:__ chemin vers le fichier owl ne contenant que la structure;  
- [ ] __DEPOSIT_IN:__ chemin vers le dépôt des fichiers json;
- [ ] __DEPOSIT_OUT:__ chemin vers le dépôt du fichier owl;
- [ ] __OWL_FILE:__ nom du fichier owl contenant les données;
- [ ] __SERVER:__ adresse et port pour l'accès au service;
- [ ] __DB_PATH:__ base de données des requêtes sauvegardées;


ONTOLOGY_BASE=./\_temp/sio\_ontology2.owl

DEPOSIT_IN=./\_temp/

DEPOSIT_OUT=./\_temp/

OWL_FILE=test-toto.owl

SERVER=0.0.0.0:5000

DB\_PATH=\_temp/QUERIES.db

### Mise en service

Depuis le dossier project12/, exécuter la commande:

```
FLASK_APP=api.py flask run
```
ou
```
python3 api.py
```

---
