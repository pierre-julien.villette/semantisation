# API Reference

---
__Endpoint :__ /owl/add_file

__Méthode :__ GET

__Description :__ Actualise l'ontologie avec un fichier json

__Paramètres :__ 

| Name      | Description                | Optionnel |
|-----------|----------------------------|-----------|
| file_name | nom du fichier             | |
| reasoner  | contrôle de la consistance | X |


__Accept :__ application/json

__Requête :__

```
> GET /owl/add_file?file_name=test.json&reasoner=False HTTP/1.1
> Host: 127.0.0.1:5000
> User-Agent: curl/7.82.0
> Accept: */*
>
```

__Réponse :__

```
< HTTP/1.1 200 OK
< Server: Werkzeug/2.2.3 Python/3.11.2
< Date: Tue, 21 Mar 2023 14:43:58 GMT
< Content-Type: application/json
< Content-Length: 21
< Connection: close
<
{
  "result": true
}
```
___

__Endpoint :__ /sparql/query

__Méthode :__ GET, POST

__Description :__ Exécute une requête sparql

__Paramètres :__ 

| Name              | Description                        | Optionnel |
|-------------------|------------------------------------|-----------|
| query             | requête                            | X         |
| **kwargs          | éventuels paramètres de la requête | X         |
| query_name        | nom de la requête                  | X         |
| query_description | description de la requête          | X         |
| query_save        | requête à enregistrer              | X         |
| query_remove      | requête à supprimer                | X         |

__Accept :__ application/json

### Exécution d'une requête avec paramètres

__Requête :__
```
curl -v --location '127.0.0.1:5000/sparql/query' \
--form 'query="PREFIX term: <http://purl.org/dc/terms/>
    SELECT ?document (COUNT(?citing_document) AS ?citation_count)
    WHERE {{
        ?document {variable} ?citing_document .
    }}
    GROUP BY ?document
    ORDER BY DESC(?citation_count)
    LIMIT 10"' \
--form 'variable="term:bibliographicCitation"'
```

```
> POST /sparql/query HTTP/1.1
> Host: 127.0.0.1:5000
> User-Agent: curl/7.82.0
> Accept: */*
> Content-Length: 508
> Content-Type: multipart/form-data; boundary=------------------------597a8a3236b311e0
>
```

__Réponse :__

```
< HTTP/1.1 200 OK
< Server: Werkzeug/2.2.3 Python/3.11.2
< Date: Tue, 21 Mar 2023 14:52:19 GMT
< Content-Type: application/json
< Content-Length: 1120
< Connection: close
<
{
  "0": [
    "http://www.semanticweb.org/vpj/ontologies/2023/2/untitled-ontology-47#document29",
    "7"
  ],
  ...
```

### Enregistrement d'une requête

__Requête :__
```
curl -v --location '127.0.0.1:5000/sparql/query' \
--form 'query="PREFIX term: <http://purl.org/dc/terms/>
    SELECT ?document (COUNT(?citing_document) AS ?citation_count)
    WHERE {{
        ?document {variable} ?citing_document .
    }}
    GROUP BY ?document
    ORDER BY DESC(?citation_count)
    LIMIT 10"' \
--form 'query_name="myQuery"' \
--form 'query_save="True"' \
--form 'query_desc="myDescription"'
```

```
> POST /sparql/query HTTP/1.1
> Host: 127.0.0.1:5000
> User-Agent: curl/7.82.0
> Accept: */*
> Content-Length: 706
> Content-Type: multipart/form-data; boundary=------------------------95b3591db8327806
>
```

__Réponse :__

```
< HTTP/1.1 200 OK
< Server: Werkzeug/2.2.3 Python/3.11.2
< Date: Tue, 21 Mar 2023 14:57:04 GMT
< Content-Type: application/json
< Content-Length: 25
< Connection: close
<
{
  "myQuery": "saved"
}
```

### Exécution d'une requête enregistrée

__Requête :__
```
curl -v --location '127.0.0.1:5000/sparql/query' \
--form 'query_name="myQuery"' \
--form 'variable="term:bibliographicCitation"'
```

```
> POST /sparql/query HTTP/1.1
> Host: 127.0.0.1:5000
> User-Agent: curl/7.82.0
> Accept: */*
> Content-Length: 275
> Content-Type: multipart/form-data; boundary=------------------------766894ed13d71f66
>
```

__Réponse :__

```
< HTTP/1.1 200 OK
< Server: Werkzeug/2.2.3 Python/3.11.2
< Date: Tue, 21 Mar 2023 14:59:36 GMT
< Content-Type: application/json
< Content-Length: 1120
< Connection: close
<
{
  "0": [
    "http://www.semanticweb.org/vpj/ontologies/2023/2/untitled-ontology-47#document29",
    "7"
  ],
  ...
```

### Suppression d'une requête enregistrée

__Requête :__
```
curl -v --location '127.0.0.1:5000/sparql/query' \
--form 'query_name="myQuery"' \
--form 'query_remove="True"'
```

```
> POST /sparql/query HTTP/1.1
> Host: 127.0.0.1:5000
> User-Agent: curl/7.82.0
> Accept: */*
> Content-Length: 257
> Content-Type: multipart/form-data; boundary=------------------------4218d2e07b60c975
>
```

__Réponse :__

```
< HTTP/1.1 200 OK
< Server: Werkzeug/2.2.3 Python/3.11.2
< Date: Tue, 21 Mar 2023 15:02:04 GMT
< Content-Type: application/json
< Content-Length: 27
< Connection: close
<
{
  "myQuery": "removed"
}
```
___

__Endpoint :__ /sparql/list_queries

__Méthode :__ GET

__Description :__ Liste les requêtes enregistrées

__Paramètres :__ Néant

__Accept :__ application/json

__Requête :__

```
> GET /sparql/list_queries HTTP/1.1
> Host: 127.0.0.1:5000
> User-Agent: curl/7.82.0
> Accept: */*
>
```

__Réponse :__

```
< HTTP/1.1 200 OK
< Server: Werkzeug/2.2.3 Python/3.11.2
< Date: Tue, 21 Mar 2023 15:07:28 GMT
< Content-Type: application/json
< Content-Length: 33
< Connection: close
<
{
  "myQuery": "myDescription"
}
```

___

__Endpoint :__ /sparql/view_query

__Méthode :__ GET, POST 

__Description :__ Affiche une requête enregistrée

__Paramètres :__ 

| Name              | Description                        | Optionnel |
|-------------------|------------------------------------|----------|
| query_name        | nom de la requête                  |          |

__Accept :__ application/json

__Requête :__

```
> GET /sparql/view_query?query_name=myQuery HTTP/1.1
> Host: 127.0.0.1:5000
> User-Agent: curl/7.82.0
> Accept: */*
>
```

__Réponse :__

```
< HTTP/1.1 200 OK
< Server: Werkzeug/2.2.3 Python/3.11.2
< Date: Tue, 21 Mar 2023 15:20:05 GMT
< Content-Type: application/json
< Content-Length: 255
< Connection: close
<
"PREFIX term: <http://purl.org/dc/terms/>
SELECT ?document (COUNT(?citing_document) AS ?citation_count)
WHERE {{
        ?document {variable} ?citing_document .
       }}
GROUP BY ?document
ORDER BY DESC(?citation_count)
LIMIT 10"
```

___